<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userInfo extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name', 'email', 'id'
  ];
public $timestamps = false;
}
