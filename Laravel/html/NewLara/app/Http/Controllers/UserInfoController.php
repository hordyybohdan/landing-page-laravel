<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\userInfo;

class UserInfoController extends Controller
{
  public function save(Request $request)
  {
    $usrInfo = new userInfo();

    $usrInfo->name = $request->name;
    $usrInfo->email = $request->email;
    $usrInfo->id = $request->id;

    $usrInfo->save();

    return json_encode([
           'success'=>true,
           'name'=>$request->name,
           'email' => $request->email,
           'id'=> $request->id
       ]);

  }
  public function show()
  {
    $users = DB::table('User_information')->get();

    return view('indexadmin', ['User_information' => $users]);
  }
}
