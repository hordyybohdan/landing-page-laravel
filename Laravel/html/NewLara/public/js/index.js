 "use strict";
class Index {

  constructor(){
    this.emailFromInput=document.getElementById('input-email').value;
    this.nameFromInput=document.getElementById('input-name').value;;
  }

  getEmail(){
    return this.emailFromInput;
  }

  getName(){
    return this.nameFromInput;
  }
}

var send = function(){
  let GetInpt = new Index();
  fetch('save',{
      method: 'post',
      headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
               },
      body: JSON.stringify({
          name: GetInpt.getName(),
          email: GetInpt.getEmail()
      })
  });
}
