@include('headF')
@yield('head')

@include('headerF')
@yield('header')

@include('mainF')
@yield('main')

@include('footerF')
@yield('footer')
