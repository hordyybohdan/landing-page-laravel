@section('admin')
  <table>
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($orders as $order)
      <tr>
        <td>{{ $order->name }}</td>
        <td>{{ $order->email }}</td>
      </tr>
    @endforeach
    </tbody>
  </table>
@endsection
